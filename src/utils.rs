
use serde::{Deserialize, Serialize};

pub type BonkResult = BonkResultWithType<()>;

pub type BonkResultWithType<T> = Result<T, Box<dyn std::error::Error>>;

#[derive(Serialize, Deserialize)]
pub struct BoardInfo {
    pub name: String,
    pub title: String,
}

impl BoardInfo {

    pub fn new() -> Self {
        Self {
            name: String::new(),
            title: String::new(),
        }
    }

    pub fn from(name: impl Into<String>, title: impl Into<String>) -> BoardInfo {
        BoardInfo {
            name: name.into(),
            title: title.into(),
        }
    }
}