
mod save;
mod utils;

#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;

use crate::save::save_unknown;
use crate::utils::BoardInfo;
use tokio::fs::File;
use crate::save::save;
use crate::save::create_save_file;
use crate::utils::BonkResult;
use fantoccini::Locator;
use fantoccini::Client;
use fantoccini::ClientBuilder;
use async_recursion::async_recursion;
use tokio::time::{sleep, Duration};

const MAX_LENGTH: usize = 10;
const CLOUDFLARE_WAIT_TIME: u64 = 5000;
const INBETWEEN_PAGE_DELAY: u64 = 100;
const WEBDRIVER_URL: &str = "http://localhost:4444";

const BASE_URL: &str = "https://boards.4chan.org/";
const SAVE_FILE: &str = "boards.txt";


lazy_static! {
    static ref ALPHANUMERIC_LOWERCASE: Vec<char> = {
        let mut range = ('a'..='z').collect::<Vec<char>>();
        let mut range2 = ('0'..='9').collect::<Vec<char>>();
        range.append(&mut range2);
        range
    };
}



#[tokio::main]
async fn main() -> BonkResult {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info")
    );

    let mut client = ClientBuilder::native().connect(WEBDRIVER_URL).await?;
    let mut file = create_save_file(SAVE_FILE).await?;

    bypass_cloudflare(&mut client, Duration::from_millis(CLOUDFLARE_WAIT_TIME)).await?;

    try_all_combinations(&mut client, &mut file).await?;
    cleanup(&mut client).await
}

async fn cleanup(client: &mut Client) -> BonkResult {
    client.close().await?;
    Ok(())
}

async fn bypass_cloudflare(client: &mut Client, wait_time: Duration) -> BonkResult {
    client.goto(BASE_URL).await?;
    sleep(wait_time).await;
    Ok(())
}

async fn try_all_combinations(client: &mut Client, file: &mut File) -> BonkResult {
    for length in 1..=MAX_LENGTH {
        let mut word =  vec!['a'; length];
        try_all_combinations_for_length(length, &mut word, client, file).await?;
    }
  
    Ok(())
}

#[async_recursion]
async fn try_all_combinations_for_length(length: usize, word: &mut Vec<char>, client: &mut Client, file: &mut File) -> BonkResult {
    let index = word.len() - length;
    for c in ALPHANUMERIC_LOWERCASE.iter() {
        word[index] = *c;
        if index == word.len() - 1 {
            let word_string: String = word.iter().collect();
            check_word(&word_string, client, file).await?;
        } else {
            try_all_combinations_for_length(length - 1, word, client, file).await?;
        }
    }
    Ok(())
}

async fn check_word(word_str: &str, client: &mut Client, file: &mut File) -> BonkResult {
    let url = format!("{}/{}", BASE_URL, word_str);
    client.goto(&url).await?;


    if let Ok(mut title) = client.find(Locator::Css(".boardTitle")).await {
        let title_text = match title.text().await {
            Ok(text) => text,
            Err(_) => String::from("No Title Text"),
        };
        let board_info = BoardInfo::from(word_str, &title_text);
        save(&board_info, file).await?;
    } else if let Ok(mut title) = client.find(Locator::Css(".boxbar")).await {
        let title_text = match title.text().await {
            Ok(text) => text,
            Err(_) => String::from("No Title Text"),
        };
        if title_text != "404 Not Found" {
            save_unknown(word_str, file).await?;
        }
    } else {
        save_unknown(word_str, file).await?;
    }

    sleep(Duration::from_millis(INBETWEEN_PAGE_DELAY)).await;
    Ok(())
}