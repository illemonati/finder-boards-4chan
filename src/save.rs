

use crate::BoardInfo;
use crate::save::fs::OpenOptions;
use crate::utils::BonkResultWithType;
use tokio::fs::{self, File};
use tokio::io::{AsyncWriteExt};
use crate::BonkResult;

pub async fn create_save_file(filename: &str) -> BonkResultWithType<File>{
    fs::remove_file(filename).await?;
    let file = OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(filename)
        .await?;
    Ok(file)
}

pub async fn save_unknown(board_name: &str, file: &mut File) -> BonkResult {
    save(&BoardInfo::from(board_name, "unknown"), file).await
}

pub async fn save(board_info: &BoardInfo, file: &mut File) -> BonkResult {
    info!("/{}/ : {}", board_info.name, board_info.title);
    let save_str = serde_json::to_string(board_info)?;
    file.write(save_str.as_bytes()).await?;
    file.write(b"\n").await?;
    Ok(())
}